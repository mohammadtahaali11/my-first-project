 class BinaryTreeNode:
    def __init__(self,data):
        self.data=data
        self.left=None
        self.right=None

    def add_child(self,data):
        if(self.data==data):
            return
        if(data<self.data):
            if(self.left):
                self.left.add_child(data)
            else:
                self.left=BinaryTreeNode(data)
        else:
            if(self.right):
                self.right.add_child(data)
            else:
                self.right=BinaryTreeNode(data)
          
    def in_order_traversal_order(self):
        elements=[]
        
        if(self.left):
            elements+=self.left.in_order_traversal_order()
            
        elements.append(self.data)
        
        if(self.right):
            elements+=self.right.in_order_traversal_order()
        
        return elements
    
    def search(self,val):
        if(self.data==val):
            return(True)
        if(val<self.data):
            if(self.left):
                return self.left.search(val)
            else:
                return(False)
        if(val>self.data):
            if(self.right):
                return self.right.search(val)
            else:
                return(False)
    def find_min(self):
        if(self.left is None):
            return(self.data)
         
        return(self.left.find_min())
        
    def find_max(self):
        if(self.right is None):
            return(self.data)
        if(self.right):
            return(self.right.find_max())
    
    def delete_right_min(self,val):
        if(val<self.data):
            if(self.left):
                self.left=self.left.delete(val)
        elif(val>self.data):
            if(self.right):
                self.right=self.right.delete(val)
        else:
            if(self.left is None and self.right is None):
                return(None)
            if(self.left is None):
                return self.right
            if(self.right is None):
                return(self.right)
             
            min_val=self.right.find_min()
            self.data=min_val
            self.right=self.right.delete(min_val)
        
        return self
    def delete_left_max(self,val):
        if(val<self.data):
            if(self.left):
                self.left=self.left.delete_left_max(val)
        elif(val>self.data):
            if(self.right):
                self.right=self.right.delete_left_max(val)
        else:
            if(self.left is None and self.right is None):
                return(None)
            if(self.left is None):
                return self.right
            if(self.right is None):
                return(self.right)
             
            max_val=self.left.find_max()
            self.data=max_val
            self.left=self.left.delete_left_max(max_val)
        
        return self
    
def build_tree(elements):
    root=BinaryTreeNode(elements[0])
    for i in range(1,len(elements)):
        root.add_child(elements[i])
    return(root)

if __name__=='__main__':
    numbers=[17,4,1,29,9,23,18,34,18,4]
    numbers_tree=build_tree(numbers)
    print(numbers_tree.in_order_traversal_order())
    print(numbers_tree.delete_left_max(17   ))
    print(numbers_tree.in_order_traversal_order()) 
        
        
        
        
        
        
        
        
        